# TP2 : Explorer et manipuler le système
```bash 
```

```


🌞 **Changer le nom de la machine**

  alienor@alienor-VirtualBox:/Desktop$
  sudo hostname node1.tp2.linux
  alienor@node1:/Desktop$ 
  sudo nano /etc/hostname
 

🌞 **Config réseau fonctionnelle**

- depuis la VM : `ping 1.1.1.1` fonctionnel
ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data
64 bytes from 1.1.1.1: icmp_seq=2 ttl=56 time=48.0 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=56 time=24.6 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=56 time=22.1 ms

  - depuis la VM : `ping ynov.com` fonctionnel
ping ynov.com
PING ynov.com (92.243.26.143) 56(84) bytes of data.
64 bytes from wvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time 17.6 ms
64 bytes from wvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time 19.1 ms

  - depuis votre PC : `ping <IP_VM>` fonctionnel
ping 192.168.132.14
64 bytes from 192.168.132.14: icmp_seq=1 ttl=64 time=0.024 ms
64 bytes from 192.168.132.14: icmp_seq=2 ttl=64 time=0.046 ms

---------






🌞 Installer le paquet openssh-server et le lancer

sudo apt install openssh-server
Reading package lists... Done
Building dependency tree.... Done
Reading state informtion... Done
openssh-server is already the newest version (1:8.4p1-6ubuntu2).
0 upgraded, 0 newly installed, 0 to remove and 1 not upgraded.

ssh 
usage: ssh [-46AaCfGgKMNnqsTVvXxTy] [-B bind_interface]
[-b bind_adress] [-c cipher_spec] [-D [bind_adress:port]]

/ect/ssh/
bash: /ect/ssh/: Is a directory
systemctl start ssh

systemctl status 
node1.tp2.linux
  State: running
  Jobs: 1 queued
  Failed: 0 units
  Since: Tue 2021-11-02 17:42:08 CET; 30min ago
  CGroup: /
        user.slice
         user-1000.slice
          user@1000.service
          session.slice


🌞 Analyser le service en cours de fonctionnement

ps
PID TTY TIME CMD
1313 pts/0 00:00:00 bash
1502 pts/0 00:00:00 ps

ss -
-K, --kill forcibly close sockets, displau xhat was closed
-H --no-header Suppress header line 
-O --oneline socket's data printed on a single line 
-- inet-sockopt show various inet socket options

journalctl
oct. 19 16:39:16 alienor-VirtualBox kermel: BIOS-e820: [mem 0*000000fee->



🌞 Connectez vous au serveur

ssh 
usage. ssh [-46AaCGgkKMNnqsTtvXxYy] [-B bind interface]
[-b bind_adress] [-c cipher_spec] [-D [bind_adress:]port]
[-E log_file] [-e escape_char] [-F configfile] [-I pkcs11]





🌞 Modifier le comportement du service

nano /etc/ssh/sshd_config
#Port 1034

cat

ss -1

systemctl restart 


🌞 Installer le paquet vsftpd

sudo apt install vsftpd 
Reading package lists.... Done
Building dependency tree.. Done
Reading state information... Done
The following NEW packages will be installed:
 vsftpd
 0 upgraded, 1 newly installed, 0 to remove and 58 not upgraded. 


🌞 Lancer le service vsftpd


systemctl start 
too few arguments.

systemctl status
node1.tp2.linux
 State: running
 Jobs: 0 queued
 Failed: 0 units


🌞 Analyser le service en cours de fonctionnement

systemctl status
node1.tp2.linux
 State: running
 Jobs: 0 queued
 Failed: 0 units

ps 
PID TTY TIME CMD
1313 pts/0  00:00:00 bash
17873 pts/0 00:00:00 ps 


ss -1


journalctl 
oct. 19 16:38:16 alienor-VirutalBox kernel: kmv-clock: Using sched offset of

/var/log/
bash: /var/log/: Is a directory


🌞 Connectez vous au serveur

ip a
192.168.132.14

ftp 192.168.132.14
connecté à 192.168.132.14.
220 (vsFTPd 3.0.3)
200 Always in UTF8 mode.
Utilisateur (192.168.132.14:(none)) : alienor
331 Please specify the password. 
Mot de passe:
230 Login successful. 
ftp>


ftp> cd uploads
550 Failed to change directory. 
ftp>put c:\files\file1.txt

ftp>get file1.txt
PORT command successful.Consider using PASV.
Failed to open file. 





🌞 Visualiser les logs

journalctl
/var/log/


🌞 Modifier le comportement du service

nano /etc/vsftpd.conf


systemctl restart












